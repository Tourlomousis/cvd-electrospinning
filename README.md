Served here:
https://tourlomousis.pages.cba.mit.edu/cvd-electrospinning 

## Challenge


## Links for Srugical Masks, N95, Respirators etc
- https://www.fda.gov/medical-devices/personal-protective-equipment-infection-control/n95-respirators-and-surgical-masks-face-masks

## Electrospinning Apparatus
### System Components & Process Parameters
- Syringe Pump
    - vol. flow rate: 0.1 - 2 mL/h
- Emitter (Hypodermic Needle)
    - Gauge: 27G / 21G
    - Preferable flat tip
- Syringe
    - 5 mL/ 3 mL plastic
    - Luer lock (Do not use needle with slip-on design)
- DC Voltage Supply
    - Voltage: 0 to 30 kV
    - Current: 0.5 mA to 1.5 mA
    - Note: Request for output cable.Ensure there is an earth terminal on the instrument
- Emitter Tip to Collector Distance
    - 0 - 12 cm 
     
### Syringe Pump

